package org.nype.odata.olingo.triton.model;

import java.util.*;
import org.apache.olingo.odata2.api.annotation.edm.*;

@EdmEntityType
@EdmEntitySet(name = "Brands")
public class Brand {
	
	@EdmKey
	@EdmProperty
	private Integer id;
	
	@EdmProperty
	private String name;
	
	@EdmNavigationProperty(name="Models")
	private List<Model> models = new ArrayList<Model>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}

}