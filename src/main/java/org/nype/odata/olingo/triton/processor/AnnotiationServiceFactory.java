package org.nype.odata.olingo.triton.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.olingo.odata2.annotation.processor.api.AnnotationServiceFactory;
import org.apache.olingo.odata2.annotation.processor.core.datasource.DataStore;
import org.apache.olingo.odata2.annotation.processor.core.datasource.DataStore.DataStoreException;
import org.apache.olingo.odata2.api.ODataService;
import org.apache.olingo.odata2.api.ODataServiceFactory;
import org.apache.olingo.odata2.api.exception.ODataApplicationException;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.api.processor.ODataContext;
import org.nype.odata.olingo.triton.model.Brand;
import org.nype.odata.olingo.triton.model.Model;

public class AnnotiationServiceFactory extends ODataServiceFactory {

	  /**
	   * Instance holder for all annotation relevant instances which should be used as singleton
	   * instances within the ODataApplication (ODataService)
	   */
	  private static class AnnotationInstances {
	    final static String MODEL_PACKAGE = "org.nype.odata.olingo.triton.model";
	    final static ODataService ANNOTATION_ODATA_SERVICE;

	    static {
	      try {
	        ANNOTATION_ODATA_SERVICE = AnnotationServiceFactory.createAnnotationService(MODEL_PACKAGE);
	      } catch (ODataApplicationException ex) {
	        throw new RuntimeException("Exception during sample data generation.", ex);
	      } catch (ODataException ex) {
	        throw new RuntimeException("Exception during data source initialization generation.", ex);
	      }
	    }
	  }

	  public ODataService createService(final ODataContext context) throws ODataException {
		  
		  createSampledata();
		  
	    return AnnotationInstances.ANNOTATION_ODATA_SERVICE;
	  }

	private void createSampledata() throws DataStoreException {
		DataStore<Brand> brands = DataStore.createInMemory(Brand.class);
		DataStore<Model> models = DataStore.createInMemory(Model.class);
		
		List<Model> sampleModels = new ArrayList<Model>();
		Model model = new Model();
		model.setName("Q5");
		sampleModels.add(model);
		models.create(model);
		
		model = new Model();
		model.setName("A6");
		sampleModels.add(model);
		models.create(model);
		
		Brand brand = new Brand();
		brand.setName("Audi");
		brand.setModels(sampleModels);
		brands.create(brand);
		
		sampleModels = new ArrayList<Model>();
		model = new Model();
		model.setName("x60");
		sampleModels.add(model);
		models.create(model);
		
		model = new Model();
		model.setName("x90");
		sampleModels.add(model);
		models.create(model);
		
		model = new Model();
		model.setName("v60");
		sampleModels.add(model);
		models.create(model);
		
		brand = new Brand();
		brand.setName("Volvo");
		brand.setModels(sampleModels);
		brands.create(brand);
	}
	
	}