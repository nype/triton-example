package org.nype.odata.olingo.triton.model;

import org.apache.olingo.odata2.api.annotation.edm.*;

@EdmEntityType
@EdmEntitySet(name = "Models")
public class Model {
	
  @EdmKey
  @EdmProperty
  private Integer id;
  
  @EdmProperty
  private String name;
  
  @EdmNavigationProperty(name="Brand")
  private Brand brand;

public Integer getId() {
	return id;
}

public void setId(Integer id) {
	this.id = id;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public Brand getBrand() {
	return brand;
}

public void setBrand(Brand brand) {
	this.brand = brand;
}
  
}
